package xpug.kata.birthday_greetings;

import static java.util.Arrays.asList;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Buffered implements BufferedService {

	private String filename ;

	public Buffered(String filename) {
		this.filename=filename;
	}
	
	/* (non-Javadoc)
	 * @see xpug.kata.birthday_greetings.EmailService#sendMessage(java.lang.String, xpug.kata.birthday_greetings.Employee)
	 */
	@Override
	public String getLine()
		BufferedReader in = new BufferedReader(new FileReader(this.filename));
		String str = "";
        Bool res;
		
		str = in.readLine();
        return str;
	}


}
